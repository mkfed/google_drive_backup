"""
Backup files from computer to google drive. Important things like which 
  directories to scan are taken from the config file set by CONFIG_PATH. 
Note about getting ID from google drive folders/files:
    id references can easily be taken from the URLs of the folders when 
    accessing google drive on the web, eg:
        https://drive.google.com/drive/u/3/folders/0F7y4vZmsEjI6LTdBRUxSXmtyObE
        the id of this folder is: 0F7y4vZmsEjI6LTdBRUxSXmtyObE
"""

import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import hashlib
import json

def create_gdrive_file(parent_folder, file_path, drive):
    """Create a new file in specified google drive directory
    
    INPUT:
      parent_folder - parent folder google drive object or ID
      file_path -  full path to file to upload
      drive -  GoogleDrive object
    OUTPUT:
      folder - GoogleDriveFile object of folder
    """
    try:
        fid = parent_folder['id']
    except:
        fid = parent_folder
    gdrive_file = drive.CreateFile({'title': os.path.basename(file_path),
                                  'parents':[{u'id': fid}]})
    gdrive_file.SetContentFile(file_path)
    gdrive_file.Upload()
    return gdrive_file

def create_gdrive_folder(parent_folder, folder_name, drive):
    """Create a new folder with name: folder_name in specified google drive 
    directory
    
    INPUT:
      parent_folder - parent folder google drive object or ID
      folder_name -  name of folder to create
      drive -  GoogleDrive object
    OUTPUT:
      folder - GoogleDriveFile object of folder
    """
    try:
        fid = parent_folder['id']
    except:
        fid = parent_folder
    folder_metadata = {"parents": [{"kind": "drive#fileLink", 
                                    "id": fid}],
                       'title' : '{}'.format(folder_name),
                       'mimeType' : 'application/vnd.google-apps.folder'}
    folder = drive.CreateFile(folder_metadata)
    folder.Upload()
    return folder


def create_gdrive_folder_a(parent_folder, folder_name, drive):
    """Return the google drive file object assosciated with the computer_name
    backup folder. Create this folder if it doesn't yet exist.
    """
    try:
        fid = parent_folder['id']
    except:
        fid = parent_folder
    gdrive_files = list_gfolder(fid, drive)
    for f in gdrive_files:
        if not isfolder(f):
            continue
        if f['title'] == folder_name:
            return f
    else:
        return create_gdrive_folder(parent_folder, folder_name, drive)


def exists_locally(gdrive_file, local_folders, rel_dir, gfolders):
    """Check that the gdrive folder exists locally. If it does update the 
    gfolders dictionary with the new entry gfolders[rel_dir] = gdrive_file
    If a match is found then remove this entry from local_folders
    
    INPUT:
        gdrive_file - google drive file object of the folder
        local_folders - list of local folders
        rel_dir - relative directory (compared to the root directory)
        gfolders - dictionary with relative directories as keys and google 
          drive folder links as the corresponding values
    OTUPUT:
        boolean - True if the google drive folder exists in the local directory        
    """
    for folder in local_folders:
        if folder == gdrive_file['title']:
            rel_dir = os.path.join(rel_dir, folder)
            gfolders[rel_dir] = gdrive_file
            local_folders.remove(folder)
            return True
    return False

def initialise_root_directory(groot_folder, computer_name, drive):
    """Return the google drive file object assosciated with the computer_name
    backup folder. Create this folder if it doesn't yet exist.
    """
    gdrive_files = list_gfolder(groot_folder, drive)
    for f in gdrive_files:
        if not isfolder(f):
            continue
        if f['title'] == computer_name:
            return f
    else:
        return create_gdrive_folder(groot_folder, computer_name, drive)

def isfolder(f):
    """Check whether the google drive item is a folder
    
    INPUT: 
        f - GoogleDriveFile
    OUTPUT:
        True if is a folder, False if is not
    """
    if f['mimeType'] == 'application/vnd.google-apps.folder':
        return True
    return False

def list_gfolder(folder_id, drive):
    """Return a list of GoogleDriveFile objects of the folder contents
    
    INPUT:
        folder_id - the google drive ID of the folder 
        drive - the google drive object
    """
    try:
        folder_id = folder_id['id']
    except:
        pass
    query = "'{}' in parents and trashed=false".format(folder_id)    
    f_list = drive.ListFile({'q':  query}).GetList()
    return f_list

def modified_checksum(gdrive_file, local_file):
    """Determine if the checksum is different between the local and remote 
    files based on md5 checksum.
    INPUT:
        pydrive_file - the file object (dictionary) which has a 'md5Checksum' 
                       key
        local_file - directory/filename for local file
    """
    with open(local_file, 'rb') as f:
        local_file_checksum = hashlib.md5(f.read()).hexdigest()
    gdrive_file_checksum = gdrive_file['md5Checksum']
    
    if local_file_checksum == gdrive_file_checksum:
        return False
    return True

def parse_config(config_file_path):
    """JSON file containing configuration options
    """
    with open(config_file_path,'r') as f:
        cf = json.load(f)
    return cf

def pydrive_start_server():
    """Open a web browser to authenticate google drive
    
    """
    gauth = GoogleAuth()
    # Create local webserver and auto handles authentication.
    gauth.LocalWebserverAuth() 
    drive = GoogleDrive(gauth)
    return drive

def remove_leading_slash(string):
    """Remove leading backslash from a string
    """
    valid_str = r'\a'
    if string and string[0] == valid_str[0]:
        return string[1:]
    return string

def update_file_upload(gdrive_file, local_file):
    """ Upload the new file if the md5checksum is different
    """
    if modified_checksum(gdrive_file, local_file):
        gdrive_file.SetContentFile(local_file)
        gdrive_file.Upload()
        return True
    return False

def update_gfile(root, local_files, gdrive_file, max_file_size=1e8):
    """Check local folder for file found on google drive. If the file exists 
    locally then update it. If the file does not exist locally delete it.
    
    INPUT:
        root - root directory of local files 
        local_files - list of files in the root directory to scan through
        gdrive_file - google drive file object
        max_file_size - (bytes) maximum file size
    OUTPUT:
        string of the action it performed useful for logging
    """
    for local_file in local_files:
        if local_file == gdrive_file['title']:
            local_path = os.path.join(root, local_file)
            local_files.remove(local_file)
            if os.stat(local_path).st_size > max_file_size:
                return 'File is too large to sync'
            
            updated = update_file_upload(gdrive_file, local_path)
            
            if updated:
                return 'Updated'
            return 'Skipped'   
    else:
        gdrive_file.Trash()
        return 'Deleted'
