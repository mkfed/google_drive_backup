gdrive_backup is a way to backup files to google drive.

Getting Started:

Install PyDrive - https://pythonhosted.org/PyDrive/
conda install -c conda-forge pydrive

Create a project on https://code.google.com/apis/console/
for detailed instructions see https://updraftplus.com/support/configuring-google-drive-api-access-in-updraftplus/

Enter the following into client_secrets.json:
    project id, 
    client id, 
    client secret, 
    redirect uri,
    javascript_origins
      
Enter the following into config.json
 "directories": list of directories to backup (os.walk will capture subdirectories)
 "computer_name": give the computer a name and the backup will be in this folder on google drive
 "gdrive_backup_folder_id": example "0F7y4vZmsEjI6LTdBRUxSXmtyObE" is the ID of the parent folder of the backup
 "logfile": name of a logfile
 "excluded": list any directories to be skipped
 "force": list any files over the file_size limit that should be backed up
 "file_size_limit" bytes - do not sync files larger than this filesize


Run main.py to backup the files. 

