import os
import gdrive_backup as gb
import datetime
#move these to config file
CONFIG_PATH = 'config.json'
LOGFILE = 'logfile.txt'
#Static folder ID of the correct google drive folder. This must change if the
#  folder is changed in google drive or if the google drive account is changed
#GDRIVE_BACKUP_FOLDER_ID = 'gdrive_backup_folder_id' -> moved to config file
LOGGING_LEVEL = 'm' #set to 'h' when confident that it is working properly
LOG_TO_TERMINAL = False #no need for this if program is running as a .pyw 
ABS_LARGE_FILE = 2e8 #bytes - not even forcing will sync a file this large

def log(message, priority='l', force_print_to_terminal=False):
    """
    INPUT:
        priority - 'h' high priroity (log when the program is run and any errors)
                   'm' medium (log direcories scanned through)
                   'l' low (log all files and the action taken on them)
    """
    logging_levels = {'h': 3, 'm': 2, 'l': 1}
    if logging_levels[priority] < logging_levels[LOGGING_LEVEL]:
        return
    with open(LOGFILE, 'a+') as f:
        f.write(message)
    if LOG_TO_TERMINAL or force_print_to_terminal:
        print(message)

def log_exception(e, level, aborted):
    log('\n{}\n'.format(level), 'h')
    log('{}\n'.format(e.args[0]), 'h')
    if aborted:
        log('--- BACKUP ABORTED ---\n', 'h')
if __name__ == '__main__':
    log('\n' + '-'*50 + '\n', 'h')
    nowtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    log('New backup started at {}\n'.format(nowtime), 'h')
    
    config_file = gb.parse_config(CONFIG_PATH)
    
    try:
        if int(config_file['file_size_limit']) > 0:
            if int(config_file['file_size_limit']) < ABS_LARGE_FILE:
                max_fsize = int(config_file['file_size_limit'])
            else:
                max_fsize = ABS_LARGE_FILE
        else:
            raise ValueError('File size limit in config.json is incorrect')
    except:
        raise ValueError('File size limit in config.json must be set correctly')
    raise
    log('Maximum file size is {}. Larger files will be skipped.\n'.format(max_fsize), 'h')
    
    drive = gb.pydrive_start_server()
    
    unsynced_files = []
    gfolders = {}
    gdrive_backup_folder_id = config_file['gdrive_backup_folder_id']
    ROOT_GFOLDER = gb.initialise_root_directory(gdrive_backup_folder_id,
                                                config_file['computer_name'],
                                                drive)

    for cur_root in config_file['directories']:
        log('{}\n'.format(cur_root), 'm')
        basename = os.path.basename(cur_root)
        gfolders[basename] = gb.create_gdrive_folder_a(ROOT_GFOLDER, 
                                                 basename, 
                                                 drive)
        gfolders[''] = gfolders[basename]
        for root, folders, files in os.walk(cur_root):
            for exclude_item in config_file['excluded']:
                if exclude_item in root:
                    continue
                basename = os.path.basename(exclude_item) 
                if basename in folders:
                    if os.path.dirname(exclude_item) == root:
                        folders.remove(basename)
                if basename in files:
                    if os.path.dirname(exclude_item) == root:
                        files.remove(basename)
                        
            folders = [e for e in folders] #copy to keep walking properly
            rel_dir = gb.remove_leading_slash(root[len(cur_root):])
            
            log('\n {}\n'.format(rel_dir), 'm')
            
            try:
                gfolder_list = gb.list_gfolder(gfolders[rel_dir], drive)
            except Exception as e:
                log_exception(e, level='FATAL', aborted=True)
                raise e
                
            #Check each item in google drive directory against the local 
            #  directory. Skip/update/delete where appropriate
            while len(gfolder_list) > 0:
                gfile = gfolder_list.pop()
                log('    {} - '.format(gfile['title']), 'l')
                if gb.isfolder(gfile):
                    #gb.exists_locally will UPDATE folders and gfolders despite
                    #  looking like it only checks for existence
                    flag_existence = gb.exists_locally(gfile, 
                                                       folders, 
                                                       rel_dir, 
                                                       gfolders)
                    if flag_existence:
                        log('Found\n', 'l')
                    else:
                        gfile.Trash()
                        log('Deleted\n', 'l')
                else:
                    try:
                        msg = gb.update_gfile(root, 
                                              files, 
                                              gfile,
                                              max_file_size = max_fsize)
                        log('{}\n'.format(msg), 'l')
                        if msg == 'File is too large to sync':
                            if os.path.join(root, 
                                            gfile['title']) in config_file['force']:
                                msg = gb.update_gfile(root, 
                                              [gfile['title']], 
                                              gfile,
                                              max_file_size = ABS_LARGE_FILE)
                                log('{}\n'.format(msg), 'l')
                                if msg == 'File is too large to sync':
                                    unsynced_files.append(os.path.join(root, 
                                                               gfile['title']))
                            else:
                                unsynced_files.append(os.path.join(root, 
                                                               gfile['title']))
                    except Exception as e:
                        log('{}\n'.format('NOT SYNCED'), 'l')
                        log_exception(e, level='ERROR', aborted=False)
                    
            #Once all google drive items have been checked create any new 
            #  files/folders which need to be created
            for local_file in files:
                try:
                    file_path = os.path.join(root, local_file)
                    if os.stat(file_path).st_size > max_fsize:
                        if os.stat(file_path).st_size < ABS_LARGE_FILE:
                            if file_path in  config_file['force']:
                                gb.create_gdrive_file(gfolders[rel_dir], 
                                          file_path,
                                          drive)
                                log('    {} - Created\n'.format(local_file), 
                                    'l')
                                continue
                        logmessage = '    {} - File is too large to sync\n'
                        log(logmessage.format(local_file), 'l', 
                            force_print_to_terminal=True)
                        unsynced_files.append(file_path)
                        continue
                    gb.create_gdrive_file(gfolders[rel_dir], 
                                          file_path,
                                          drive)
                    log('    {} - Created\n'.format(local_file), 'l')
                except Exception as e:
                    log('    {} - NOT SYNCED\n'.format(local_file), 'l')
                    log_exception(e, level='ERROR', aborted=False)
            
            for local_folder in folders:
                new_folder = gb.create_gdrive_folder(gfolders[rel_dir], 
                                                     local_folder, 
                                                     drive)
                gfolders[os.path.join(rel_dir, local_folder)] = new_folder
                
                log('    {} - Created (Folder)\n'.format(local_folder), 'l')
    
    if len(unsynced_files) > 0:
        log('The following files were not synced', 'h')
        for file in unsynced_files:
            log('  {}'.format(file), 'h')
